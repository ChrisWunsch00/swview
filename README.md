SWView
======

Swipe-able View. View can be swiped and has UIDynamics to animate it off screen or 'snap' back to it's origin.

Designed to be very lightweight and easy to integrate. For usage example see below:

<pre><code>//Initialse the view with a frame, I have used the current view's center as my snap point.
SWView *svView = [[SWView alloc] initWithFrame:YOUR_FRAME andSnapPoint:self.view.center fromRootView:self.view];
[self.view addSubview:svView];
[svView setupCollisionBehaviourWithDelegate:self];</code></pre>

You can then implement:

<pre><code>- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id <UIDynamicItem>)item withBoundaryIdentifier:(id <NSCopying>)identifier atPoint:(CGPoint)p
- (void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id <UIDynamicItem>)item withBoundaryIdentifier:(id <NSCopying>)identifier</code></pre>

To listen for when the view collides with the boundary.

In my implementation I do the following:

<pre><code>-(void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id <UIDynamicItem>)item withBoundaryIdentifier:(id <NSCopying>)identifier {
  //Get the SWView:
  if ([item isKindOfClass:[SWView class]]) {
    [(SWView *)item removeFromSuperview];
  }
}</code></pre>
