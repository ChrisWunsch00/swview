//
//  SWView.m
//  BouncyCollectionView
//
//  Created by Chris Watson on 08/11/2013.
//  Copyright (c) 2013. All rights reserved.
//

#import "SWView.h"

@interface SWView ()

@property (nonatomic) CGPoint snapPoint;

@end

@implementation SWView

- (id)initWithFrame:(CGRect)frame andSnapPoint:(CGPoint)snapPoint fromRootView:(UIView *)view {

    self = [super initWithFrame:frame];

    if (self) {

        self.snapPoint = snapPoint;
        self.backgroundColor = [UIColor blueColor];

        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:view];
        [_animator setDelegate:self];

        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
        [self addGestureRecognizer:pan];
    }

    return self;
}

- (void) setupCollisionBehaviourWithDelegate:(id) delegate {
    _collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[self]];
    [_collisionBehavior setCollisionMode:UICollisionBehaviorModeEverything];
    float collisionLimit = self.frame.size.width * 2;
    [_collisionBehavior setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(-collisionLimit,
            -collisionLimit, -collisionLimit, -collisionLimit)];
    [_collisionBehavior setCollisionDelegate:delegate];
}

- (void)handlePan:(UIPanGestureRecognizer *)gesture {

    CGPoint touchPoint = [gesture locationInView:self.superview];
    UIView *draggedView = [gesture view];

    if (gesture.state == UIGestureRecognizerStateBegan) {
        _draggingView = YES;
        _previousTouchPoint = touchPoint;
        _gestureBeganTime = [NSDate date];
        [_animator removeBehavior:_snap];
    }

    else if (gesture.state == UIGestureRecognizerStateChanged && _draggingView) {
        float yOffset = _previousTouchPoint.y - touchPoint.y;
        float xOffset = _previousTouchPoint.x - touchPoint.x;
        gesture.view.center = CGPointMake(draggedView.center.x - xOffset, draggedView.center.y - yOffset);
        _previousTouchPoint = touchPoint;
    }

    else if (gesture.state == UIGestureRecognizerStateEnded && _draggingView) {
        NSTimeInterval interval = [_gestureBeganTime timeIntervalSinceDate:[NSDate date]];
        if (interval < - 0.1)
            [self snapBackToPoint:draggedView];
        else
            [self addVelocityToView:draggedView fromGesture:gesture];

        [_animator updateItemUsingCurrentState:draggedView];
        _draggingView = NO;
    }
}

- (void)snapBackToPoint:(UIView *) view {
    _snap = [[UISnapBehavior alloc] initWithItem:view snapToPoint:self.snapPoint];
    [_snap setDamping:1.0];
    [_animator addBehavior:_snap];
}

- (void)addVelocityToView:(UIView *) view fromGesture:(UIPanGestureRecognizer *) gesture {

    CGPoint vel = [gesture velocityInView:self];
    vel.x = vel.x * 0.55;
    vel.y = vel.y * 0.55;

    UIDynamicItemBehavior *itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self]];
    [itemBehavior setAllowsRotation:YES];
    [itemBehavior addAngularVelocity:0.5 forItem:view];
    [itemBehavior addLinearVelocity:vel forItem:view];
    [_animator addBehavior:itemBehavior];

    if (_collisionBehavior) {
        [_animator addBehavior:_collisionBehavior];
    }
}

@end
