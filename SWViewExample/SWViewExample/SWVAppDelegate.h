//
//  SWVAppDelegate.h
//  SWViewExample
//
//  Created by Chris Watson on 11/11/2013.
//  Copyright (c) 2013 ChrisWatson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
