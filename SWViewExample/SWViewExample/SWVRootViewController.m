//
//  SWVRootViewController.m
//  SWViewExample
//
//  Created by Chris Watson on 11/11/2013.
//  Copyright (c) 2013 ChrisWatson. All rights reserved.
//

#import "SWVRootViewController.h"

#import "SWView.h"

@interface SWVRootViewController ()

@end

@implementation SWVRootViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIButton *loadView = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [loadView setFrame:CGRectMake(self.view.center.x - 100, self.view.center.y - 50, 200, 100)];
    [loadView setTitle:@"Press to Load SWView" forState:UIControlStateNormal];
    [loadView addTarget:self action:@selector(loadSWView:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:loadView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadSWView:(id) sender {
    
    SWView *swView = [[SWView alloc] initWithFrame:CGRectMake(self.view.center.x - 100, self.view.center.y - 50, 200, 100) andSnapPoint:self.view.center fromRootView:self.view];
    [[self view] addSubview:swView];
    [swView setupCollisionBehaviourWithDelegate:self];
}

- (void)collisionBehavior:(UICollisionBehavior *)behavior beganContactForItem:(id<UIDynamicItem>)item withBoundaryIdentifier:(id<NSCopying>)identifier atPoint:(CGPoint)p {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    if ([item isKindOfClass:[SWView class]]) {
         [(SWView *)item removeFromSuperview];
    }
}

@end
