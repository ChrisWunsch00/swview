//
//  SWView.h
//  BouncyCollectionView
//
//  Created by Chris Watson on 08/11/2013.
//  Copyright (c) 2013. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 `SWView` is a subclass of UIView. It has UIDynamic properties that allow it to be swiped and will 'snap' back to it's
 original position when released after a minimum time. If released before this time i.e. quickly 'swiped away' it will
 take the velocity of the swipe and 'fly' off screen whilst rotating until it is fully out of view. The collisionBehavior
 delegate can be used to detect when it reaches this state.
*/

@interface SWView : UIView <UIDynamicAnimatorDelegate> {

    ///---------------------------------------
    /// @name Accessing Swipe-able View Properties:
    ///---------------------------------------

    //Dynamic animator to hold the behaviors:
    UIDynamicAnimator *_animator;

    //UISnapBehavior property for the 'return to origin' animation:
    UISnapBehavior *_snap;

    //UICollisionBehavior to managed when the view has reached the end of it's journey:
    UICollisionBehavior *_collisionBehavior;

    //The previous touch location, used to 'drag' the view in the pan gesture recogniser.
    CGPoint _previousTouchPoint;

    //BOOL to determine whether we are dragging the view:
    BOOL _draggingView;

    //NSDate to record the start of the pan gesture. This is used to determine which behavior to follow
    //once the gesture has ended.
    NSDate *_gestureBeganTime;
}


///---------------------------------------
/// @name Accessing Swipe-able View Public Methods:
///---------------------------------------

/**
 Creates an instance of SWView and sets the basic UIDynamic properties needed.

 @param frame the frame which the view should take within it's superview.
 @param snapPoint the point at which the view should 'snap' back to when the user stops dragging.
 @param view the view in which this SWView belongs.

 @return self
 */
- (id)initWithFrame:(CGRect)frame andSnapPoint:(CGPoint)snapPoint fromRootView:(UIView *)view;

/**
 Creates the collisionBehavior and it's properties.

 @param delegate the viewController which will call the collisionBehavior delegate methods

 @return void
 */
- (void)setupCollisionBehaviourWithDelegate:(id)delegate;

@end
